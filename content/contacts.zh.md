+++
title = "联系方式"
date = 2024-01-31
template = "contact-page.html"

[extra]
links = [
  # {icon = "github", name = "My projects on Github", link = "https://github.com/adfaure/kodama-theme"},
  {icon = "gitlab", name = "Gitlab", link = "https://gitlab.oit.duke.edu/hg163"},
  {icon = "linkedin", name = "Linkedin", link = "https://www.linkedin.com/in/huanli-gong-946903293/"},
  {icon = "resume", name = "简历", link = 'resume/龚桓立简历.pdf' }
  # {icon = "goodreads", icon_pack = "fab", name = "Discuss on", link = "https://www.goodreads.com/user/show/82099752-adrien"}
  # {icon = "telegram", icon_pack = "fab", name = "Telegram Me", link = "https://telegram.me/@Telegram"},
  ]
+++
