+++
title = "工作"
date = 2024-01-31

[extra]

[[extra.list]]
  name = "福建可比信息技术有限公司"
  time = "2021.08 ~ 2021.09"
  location = '福州, 中国'
  role = "软件工程师"
  summary = ''
  details = [
    "为中国侨商联合会（CFOCE）官方网站开发了PC端和移动端的首页 API。",
    "为中文头条新媒体（CHNM）实现了查看审稿人日志的功能。",
  ]

[[extra.list]]
  name = "福建信同信息技术有限公司"
  time = "2019.07 ~ 2019.08"
  location = '厦门, 中国'
  role = "信号处理工程师"
  summary = ""
  details = [
    "为数字预失真（DPD）项目设计UI界面,并通过分析用户需求评估界面设计。",
    "为 DPD 项目创建并执行测试计划，同时设计并开发自动测试框架，以验证功能并确保系统的稳健性。",
  ]

+++
