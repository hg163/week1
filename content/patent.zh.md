+++
title = "专利"
date = 2024-01-31

[extra]

[[extra.list]]
  authors = ['龚桓立']
  date='2022-05-17'
  name = "一种能显示网络信息的路由器"
  number =  "216565201"
  kind = 'U'
  office='CH'
  url = "https://patentscope.wipo.int/search/en/detail.jsf?docId=CN362221752&_cid=P12-L7L42O-92793-1"


[[extra.list]]
  authors = ['龚桓立']
  date='2021-09-07'
  name = "一种基于RS触发器的四路抢答器"
  number =  "214151904"
  kind = 'U'
  office='CH'
  url = "https://patentscope.wipo.int/search/en/detail.jsf?docId=CN336288989&_cid=P12-L7L44A-92914-1"

[[extra.list]]
  authors = ['刘聪江','龚桓立']
  date='2021-04-27'
  name = "一种射频功率放大器内自动增益与线性控制装置及方法"
  number =  "112713861"
  kind = 'A'
  office='CH'
  url = "https://patentscope.wipo.int/search/en/detail.jsf?docId=CN323586272&_cid=P21-L7L1FD-72950-1"

+++

