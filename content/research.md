+++
title = "Research"
date = 2024-01-31

[extra]

[[extra.list]]
  name = "Amazon's Alexa Prize TaskBot Challenge 2"
  time = "2023.01 ~ 2023.09"
  location = ''
  role = "Member of The Ohio State University team"
  summary = 'Took the lead in Natural Language Understanding (NLU) subteam for ASR correction, intent classification, and question classification, and participated in the User Engagement subteam.'
  details = [
    "Developed and deployed a knowledge-based ASR error correction approach tailored for dialog scenarios, while evaluating its performance in comparison to various language models.",
    "Debugged and improved Intent Classifier, Question Classifier as well as other modules in the bot.",
    "Monitor user ratings and feedback and choose TaskBot-recommended tasks based on user interests."
  ]

[[extra.list]]
  name = "Automatic Speech Recognition (ASR) Error Correction"
  time = "2022.05 ~ 2022.12"
  location = ''
  role = "Research Assistant for Project Director (Prof. Huan Sun)"
  summary = "Proposed an approach to correct errors in ASR transcription and prepare it for Amazon's TaskBot Challenge 2."
  details = [
    "Implemented a pipeline to generate ASR errors by simulating noise and use it to obtain noisy versions of datasets.",
    "Proposed a method to improve model performance by injecting pronunciation from pronunciation encoding into the model pre-training process in addition to the meaning of words from context.",
    "Tested the performances of Language models for ASR error correction."
  ]

[[extra.list]]
  name = "Dataset for Generating Deep Questions in Education"
  time = "2021.08 ~ 2022.09"
  location = ''
  role = "Project Leader"
  summary = "Presented a dataset for models to generate deep questions on 29th International Conference on Computational Linguistics."
  details = [
    "Extracted, paraphrased, and annotated real questions and answers from Khan Academy as a dataset called KHANQ.",
    "Conducted both automatic (BLEU, METEOR, ROUGE-L) and human evaluations of SOTA text generation models including GPT, BART, and T5, concluding on challenges for models to generate human-like deep questions.",
  ]

+++
