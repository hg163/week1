+++
title = "Project"
date = 2024-01-31

[extra]

[[extra.list]]
  name = "Social Reinforcement Learning Project"
  time = "2022.05 ~ 2022.08"
  location = ''
  # role = "Mentored by Rui Ding from Microsoft (China) Co"
  role = ''
  summary = 'Modeled news spreading on social networks and utilized Multi-Agent Deep Deterministic Policy Gradient (MADDPG) to solve dynamic processes on a graph, replicated with Offline Reinforcement Learning (MARWIL).'
  details = [
  ]
[[extra.list]]
  name = "Ruby on Rails Project: Web-based Presentations Evaluation Application"
  time = "2021.09 ~ 2021.12"
  location = ''
  role = ''
  summary = 'Implemented a web application would streamline the collection, collation, and analysis of audience evaluations of presentations. Instructors can manage users, presentations, and teams, and students can submit and view feedbacks. Extended features include authentication, admin dashboard, user alerts, output formatting, PostgreSQL with Heroku.'
  details = [
  ]

[[extra.list]]
  name = "Quandary language interpreter in java"
  time = "2022.01 ~ 2022.04"
  location = ''
  role = ''
  summary = 'Implemented a Quandary language interpreter an interpreter written in Java that has all language features including function calls, mutation, heap, and concurrency.'
  details = [
  ]

+++
