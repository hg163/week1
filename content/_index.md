+++
title = "Home"
date = 2024-01-31
page_template="page.html"

[extra]
title = ""

interests = [

  "Martian food",

  "Quantic science"

]

[[extra.education.courses]]
  degree = "M.S. Computer Science"
  institution = "Duke University"
  time = "May. 2025"
  GPA = '4.0/4.0'
  Option = ''
  Minor = ''
  Honors=[]

[[extra.education.courses]]
  degree = "B.S. Computer Science and Engineering"
  institution = "The Ohio State University"
  time = 'May. 2023'
  GPA = '4.0/4.0'
  Option = 'Artificial Intelligence'
  Minor = 'Mathematics'
  Honors=["Dean's List","Summa Cum Laude"]


# [[extra.avatar_icons]]
#  icon = "github"
#  link = "https://github.com/adfaure"
# [[extra.avatar_icons]]
#  icon = "gitlab"
#  link = "https://gitlab.com/adfaure"
[[extra.avatar_icons]]
  icon = "gitlab"
  link = "https://coursework.cs.duke.edu/hg163"
[[extra.avatar_icons]]
  icon = "linkedin"
  link = "https://www.linkedin.com/in/huanli-gong-946903293/"
+++

I am Huanli Gong, a master student at [Duke University](https://duke.edu/), major in Computer Science. Before I joined Duke University, I obtained B.S. in Computer Science and Engineering from [The Ohio State University](https://www.osu.edu/) with 4.0 GPA. In addition to maintaining grades, I am also actively seeking academic and professional opportunities. Previously. I worked for [OSU NLP group](https://u.osu.edu/ihudas/people/) and published papers. I am currently interested in [kaggle](https://www.kaggle.com/huanligong) and become competitions expert (rank 3133 of 223,016). Now I am looking for a summer internship!
