+++
title = "Contact"
date = 2024-01-31
template = "contact-page.html"

[extra]
location = '510 S LaSalle St Apt 1120, Durham, NC 27705'
phone = '+ 1 (614) 967-0735'
links = [
  # {icon = "github", name = "My projects on Github", link = "https://github.com/adfaure/kodama-theme"},
  {icon = "gitlab", name = "My projects on Gitlab", link = "https://gitlab.oit.duke.edu/hg163"},
  {icon = "linkedin", name = "Chat on Linkedin", link = "https://www.linkedin.com/in/huanli-gong-946903293/"},
  {icon = "resume", name = "View my resume", link = 'resume/Huanli Gong Resume.pdf' },
  # {icon = "goodreads", icon_pack = "fab", name = "Discuss on", link = "https://www.goodreads.com/user/show/82099752-adrien"}
  # {icon = "telegram", icon_pack = "fab", name = "Telegram Me", link = "https://telegram.me/@Telegram"},
  ]
+++
